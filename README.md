Simple ros - rviz keyboard mover for the rviz_plugin_osvr plugin by Veix123: https://github.com/Veix123/rviz_plugin_osvr

Arrow keys to move and right shift/ctrl to go up and down. Keyboard rotation not yet implemented.

Just build, launch with the provided launch file. Needs to be run as root.